#!/bin/bash --
# -*- coding: utf-8; mode: shell-script; tab-width: 4 -*-

argv=("$@")

while getopts :ab: opt "${argv[@]}"; do
    case $opt in
	a)
	    echo "-a was set" ;;
	b)
	    echo "-$opt was set to \"$OPTARG\"" ;;
	\?)
	    echo "invalid option: -$OPTARG" ;;
	:)
	    echo "option -$OPTARG requires an argument"
	    exit 1 ;;
    esac
done
