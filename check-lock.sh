#!/bin/bash --
# -*- coding: utf-8; mode: shell-script; tab-width: 4 -*-

LOCKFILE=/tmp/this.pidfile

if [ -e $LOCKFILE ]; then
    while ps fax | grep "^$(cat $LOCKFILE)" > /dev/null;  do
       sleep 5
    done
fi

echo "$$" > $LOCKFILE

# ------

echo "begin job"
sleep 10
echo "end job"
